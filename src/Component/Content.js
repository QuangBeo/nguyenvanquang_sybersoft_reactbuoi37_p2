import React, { useState } from "react";
import GlassList from "./GlassList";
import { dataGlasses } from '../dataGlasses';

export default function Content() {
  const [glass, setGlass] = useState({
    id: 1,
    price: 30,
    name: "GUCCI G8850U",
    url: "./glassesImage/v1.png",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  });
  const hanldClick = (newGlass) => {
    setGlass(newGlass);
  }
  return (
    <div
      className="content"
      style={{
        background: "url(./glassesImage/background.jpg)",
        width: "100wh",
        height: "100vh",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
      }}
    >
      <div className="header p-4" style={{ background: "rgba(0,0,0,0.7)" }}>
        <h1 className="text-light">TRY CLASSES APP ONLINE</h1>
      </div>
      <div className="container">
        <div className="content d-flex justify-content-around">
          <div className="show-content position-relative">
            <img
              src="./glassesImage/model.jpg"
              alt=""
              style={{
                width: "350px",
                height: "400px",
              }}
            />
            <div
              className="content-info text-left"
              style={{
                bottom: "0",
                left: "0",
                width: "100%",
                position: "absolute",
                color: "white",
                background: "rgba(0,0,0,0.4)",
              }}
            >
              <h3>{glass.name}</h3>
              <p>{glass.desc}</p>
            </div>
            <img
              src={glass.url}
              style={{
                position: "absolute",
                top: "106px",
                width: "170px",
                left: "90px",
              }}
              alt=""
            />
          </div>
          <img
            src="./glassesImage/model.jpg"
            style={{
              width: "350px",
              height: "400px",
            }}
            alt=""
          />
        </div>
      </div>
      <div className="footer">
        <GlassList dataGlasses={dataGlasses} hanldClick={hanldClick}/>
      </div>
    </div>
  );
}
