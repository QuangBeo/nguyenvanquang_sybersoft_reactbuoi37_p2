import React, { memo } from 'react'
import { dataGlasses } from '../dataGlasses';

function GlassItem({dataGlasses,hanldClick}) {
  return (
    <img src={dataGlasses.url} alt="" className='img-fluid'
    onClick={() => {hanldClick(dataGlasses)}}/>
  )
}

export default memo(GlassItem);