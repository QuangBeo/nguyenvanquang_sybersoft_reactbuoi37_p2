import React, { memo } from 'react'
import { dataGlasses } from '../dataGlasses';
import GlassItem from './GlassItem';

function GlassList({dataGlasses,hanldClick}) {
    const renderGlasses = () => {
        return dataGlasses.map((item,index) => {
            return <div className="col-2" key={index}>
                <GlassItem dataGlasses={item} hanldClick={hanldClick}/>
            </div>
        })
    }
  return (
    <div className='row container mx-auto pt-5'>
        {renderGlasses()};
    </div>
  )
}

export default memo(GlassList);